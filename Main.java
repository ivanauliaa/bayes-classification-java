import java.util.Arrays;
import java.util.List;
import java.util.ArrayList;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

public class Main {
    public static void main(String[] args) {
        studiKasusBuah();
        // studiKasusHipertensi();
    }

    public static void studiKasusBuah() {
        List<List<String>> data_train = scanData("buah_train.txt");
        List<String> labels = getLabels(data_train);
        List<List<String>> data_test = scanData("buah_test.txt");
        List<String> result = bayesianClassification(data_train, data_test, labels);

        printResult(data_test, result);
    }

    public static void studiKasusHipertensi() {
        List<List<String>> data_train = scanData("hipertensi_train.txt");
        List<String> labels = getLabels(data_train);
        List<List<String>> data_test = scanData("hipertensi_test.txt");
        List<String> result = bayesianClassification(data_train, data_test, labels);

        printResult(data_test, result);
    }

    public static List<List<String>> scanData(String url) {
        final String COMMA_DELIMITER = ",";
        List<List<String>> records = new ArrayList<>();

        try (BufferedReader br = new BufferedReader(new FileReader(url))) {
            String line;

            while ((line = br.readLine()) != null) {
                String[] values = line.split(COMMA_DELIMITER);
                records.add(Arrays.asList(values));
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        return records;
    }

    public static List<String> getLabels(List<List<String>> data_train) {
        List<String> labels = new ArrayList<>();
        int labelIndex = data_train.get(0).size() - 1;

        for (List<String> line : data_train) {
            String trainLabel = line.get(labelIndex);

            if (labels.isEmpty()) {
                labels.add(trainLabel);
            } else {
                boolean labelExist = false;

                for (String label : labels) {
                    if (label.equals(trainLabel)) {
                        labelExist = true;
                        break;
                    }
                }

                if (!labelExist) {
                    labels.add(trainLabel);
                }
            }
        }

        return labels;
    }

    public static double getProbability(List<List<String>> data_train, String label, List<String> params) {
        if (params == null) { // nyari persentase banyaknya label per keseluruhan data
            int count = 0;
            int labelIndex = data_train.get(0).size() - 1;
            for (List<String> line : data_train) {
                if (line.get(labelIndex).equals(label)) {
                    count++;
                }
            }

            return (double) count / (double) data_train.size();
        } else { // nyari persentase banyaknya data dengan param tertentu terhadap sebuah label
                 // per banyaknya label
            double labelProbability = getProbability(data_train, label, null);
            int labelIndex = data_train.get(0).size() - 1;

            double paramProbability = 1;
            for(int i = 0; i < params.size(); i++) {
                int param_found = 0;
                int label_found = 0;

                for(List<String> train_row : data_train) {
                    if(train_row.get(labelIndex).equals(label)) {
                        if(train_row.get(i).equals(params.get(i)) && train_row.get(labelIndex).equals(label)) {
                            param_found++;
                        } 
                        label_found++;
                    }
                }
                
                paramProbability *= ((double) param_found / (double) label_found);
            }

            return paramProbability * labelProbability;
        }
    }

    public static List<String> bayesianClassification(List<List<String>> data_train, List<List<String>> data_test,
            List<String> labels) {
        // buat List<String> penampung result untuk tiap baris data_test
        List<String> result = new ArrayList<>();
        // perulangan sepanjang data_test
        for (int i = 0; i < data_test.size(); i++) {
            // buat List<String> penampung probabilitas untuk tiap data test terhadap label
            List<Double> probabilities = new ArrayList<>();
            // perulangan sepanjang labels
            for (int j = 0; j < labels.size(); j++) {
                // cari probabilitas dan return hasilnya ke sebuah var
                double probability = getProbability(data_train, labels.get(j), data_test.get(i));
                // assign hasil probabilitas ke list penampung probabilitas
                probabilities.add(probability);
            }
            // cari probabilitas terbesar
            int resultIndex = getBiggestProbabilityIndex(probabilities);
            // assign ke penampung result
            result.add(probabilities.get(resultIndex) != 0 ? labels.get(resultIndex) : "No result [any new param value]");
        }
        // return penampung result
        return result;
    }

    public static int getBiggestProbabilityIndex(List<Double> probabilities) {
        int index = 0;

        for(int i = 0; i < probabilities.size(); i++) {
            if(probabilities.get(index) < probabilities.get(i)) {
                index = i;
            }
        }

        return index;
    }

    public static void printResult(List<List<String>> data_test, List<String> result) {
        System.out.println("Result");
        for (int i = 0; i < data_test.size(); i++) {
            for (int j = 0; j < data_test.get(i).size(); j++) {
                System.out.print(data_test.get(i).get(j) + ", ");
            }
            System.out.println(" | " + result.get(i));
        }
    }
}